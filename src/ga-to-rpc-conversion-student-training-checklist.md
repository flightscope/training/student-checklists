# GA to RPC conversion student training checklist

* RA-Aus membership [form](https://www.raa.asn.au/storage/application-for-membership-flying.pdf)
* RAAus converting pilot exam
* Flight time in RAAus registered aircraft
  * ≥5 hours aeronautical experience
  * ≥1 hour solo
* RAAus Converting Pilot Certificate [form](https://www.raa.asn.au/forms/)
* Flying competency of RPC test standard
