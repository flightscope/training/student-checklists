# RPC to RPL conversion student training checklist

* Aviation Reference Number (ARN) [form 1162](https://www.casa.gov.au/sites/default/files/aviation-reference-number-application-individual-form1162.pdf)
* Aviation Security Identification Card (ASIC)
  * [Applying for an ASIC](https://www.casa.gov.au/licences-and-certification/individual-licensing/aviation-security-requirements-pilots)
  * *[See Aviation Transport Security Regulations 2005 (3.03)](classic.austlii.edu.au/au/legis/cth/consol_reg/atsr2005457/s3.03.html)*
* Flight Lessons
  * Advanced Stalling Lesson 16
  * Steep Turning Lesson 18
  * Short Field Takeoff and Landing Lesson 22
  * Basic Instrument Flight Lesson 26
  * Single Engine Aeroplane Endorsement Questionaire
* Recreational Pilot Licence (Aeroplane) Exam
  * [ASPEQ PEXO Flight Crew Examinations](https://casa.aspeqexams.com/)
* CASA Form 61-9FR Notification of Flight Review Form [form](https://www.casa.gov.au/files/form-61-9fr-notification-flight-review)
* Aeroplane Flight Rreview to be conducted by CASA Grade 2 *(or higher)* flight instructor
