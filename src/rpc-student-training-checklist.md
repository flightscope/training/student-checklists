# RPC student training checklist

### First training flight

* create student training record
  * Contact details
  * Next of kin details
  * Insurance excess form
  * Indemnity form
  * RA-Aus membership [form](https://www.raa.asn.au/storage/application-for-membership-flying.pdf)
  * Progress sheet
  * Aviation Reference Number (ARN) [form 1162](https://www.casa.gov.au/sites/default/files/aviation-reference-number-application-individual-form1162.pdf)
  * [Aviation Security Identity Card (ASIC)](https://www.casa.gov.au/licences-and-certification/individual-licensing/aviation-security-requirements-pilots)

### First solo

* CASA Class 2 *(or higher)* Basic Medical if CTA or [medical declaration](https://www.raa.asn.au/storage/medical-declaration.pdf)
* Pre-solo Operational Exam *(5 questions — local area and aircraft type)*
* Pre-solo Air Legislation Exam *(20 questions)*
* RAAus Radio Exam *required for CTA* *(30 questions)*
* English Language Proficiency exam
  * *(Required for solo during YBAF tower hours)*
* Senior Instructor flight check
* Flying competency to solo standard

### Subsequent solo

* Senior Instructor flight check

### Training area solo

* Aeronautical chart onboard aircraft
* Flying competency to area solo standard
* Senior Instructor flight check

### RPC flight test review

* ≥15 hours dual flight
* ≥5 hours solo flight
* RAAus Air Legislation Exam *(50 questions)*
* RAAus Basic Aeronautical Knowledge Exam *(60 questions)*
* Flying competency of RPC test standard
* Provide student with Flightscope Flight Test criteria [form](https://flightscope.gitlab.io/flight-testing/flight-test-rpc.pdf)
* Flightscope Theory and Flight Test Forms completed and stored in student training file

### RPC flight test

* RAAus Pilot Certificate Issue [form](https://www.raa.asn.au/storage/pilot-certificate-issue-april-2019.pdf)
* Student to bring Flightscope Flight Test criteria [form](https://flightscope.gitlab.io/flight-testing/flight-test-rpc.pdf)
* Flight Test performed by CFI or Pilot Examiner
